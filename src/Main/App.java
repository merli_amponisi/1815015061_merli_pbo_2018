package Main;

import baby_photography.Photo;
import baby_photography.Editor;
import java.util.Scanner;
 
public class App{
    public static void main(String[] args){
        //MEMBUAT SCANNER INPUT
        Scanner input = new Scanner(System.in);
        
        //MEMBUAT OBJEK BARU UNTUK CLASS PHOTOGRAPER
        Photo Photographer = new Photo();
        
        //MEMBUAT OBJEK BARU UNTUK CLASS EDITOR
        Editor Edit = new Editor();
        
        //MEMBUAT OBJEK BARU UNTUK CLASS KEAHLIAN EDITOR1
        
        
        //GO TO UNTUK KEMBALI KE MENU AWAL
        out:
        while (true){
            System.out.println(" ---------------------------------");
            System.out.println("      KARYAWAN BABY PHOTOGRAPHY");
            System.out.println(" ---------------------------------");
            System.out.println("        1. Photographer ");
            System.out.println("        2. Editor ");
            System.out.println("        3. Keahlian Editor");
            System.out.println("        4. Exit");
            System.out.println(" ---------------------------------");
            System.out.print("            Pilih  : ");
            int pilih = input.nextInt();
            switch(pilih){
                
            //MENU PHOTOGRAPHER
            case 1:
                System.out.println(" ---------------------------------");
                System.out.println("          PHOTOGRAPHY");
                System.out.println(" ---------------------------------");
                System.out.println("        1. Insert");
                System.out.println("        2. Read");
                System.out.println("        3. Update");
                System.out.println("        4. Search ID");
                System.out.println("        5. Search Nama");
                System.out.println("        6. Delete");
                System.out.println("        7. Back");
                System.out.println(" ---------------------------------");
                System.out.print("            Pilih  : ");
                int pilih1 = input.nextInt();
                switch (pilih1){
                    case 1:
                        Photographer.Insert();
                        break;
                    case 2:
                        Photographer.Read();
                        break;
                    case 3:
                        Photographer.update();
                        break;
                    case 4:
                        Photographer.SearchID();
                        break;
                    case 5:
                        Photographer.Search();
                        break;
                    case 6:
                        Photographer.Del();
                        break;
                    case 7:
                        break;
                    default:
                        System.out.println(" ---------------------------------");
                        System.out.println(" Input salah. silahkan ulangi");
                        break ;
                }
                break;
            //MENU EDITOR
            case 2:
                System.out.println(" ---------------------------------");
                System.out.println("             EDITOR");
                System.out.println(" ---------------------------------");
                System.out.println("        1. Insert");
                System.out.println("        2. Read");
                System.out.println("        3. Update");
                System.out.println("        4. Search ID");
                System.out.println("        5. Search Nama");
                System.out.println("        6. Delete");
                System.out.println("        7. Back");
                System.out.println(" ---------------------------------");
                System.out.print("            Pilih  : ");
                int pilih2 = input.nextInt();
                switch (pilih2){
                    case 1:
                        Edit.listKeahlian();
                        Edit.InsertEdit();
                        break;
                    case 2:
                        Edit.ReadEdit();
                        break;
                    case 3:
                        Edit.updateEdit();
                        break;
                    case 4:
                        Edit.SearchIDEdit();
                        break;
                    case 5:
                        Edit.SearchEdit();
                        break;
                    case 6:
                        Edit.DelEdit();
                        break;
                    case 7:
                        break;
                    default:
                        System.out.println(" ---------------------------------");
                        System.out.println(" Input salah. silahkan ulangi");
                        break;
                }
                break;
            case 3:
                System.out.println(" ---------------------------------");
                System.out.println("             KEAHLIAN");
                System.out.println(" ---------------------------------");
                System.out.println("        1. Insert");
                System.out.println("        2. Read");
                System.out.println("        3. Update");
                System.out.println("        4. Search ID");
                System.out.println("        5. Search Jenis");
                System.out.println("        6. Delete");
                System.out.println("        7. Back");
                System.out.println(" ---------------------------------");
                System.out.print("            Pilih  : ");
                int pilih3 = input.nextInt();
                switch (pilih3){
                case 1:
                    Edit.InsertKeahlian();
                    break;
                case 2:
                    Edit.ReadKeahlian();
                    break;
                case 3:
                    Edit.updateDataKeahlian();
                    break;
                case 4:
                    Edit.SearchIDKeahlian();
                    break;
                case 5:
                    Edit.SearchKeahlian();
                    break;
                case 6:
                    Edit.DelKeahlian();
                    break;
                case 7:
                    break;
                default:
                    System.out.println(" ---------------------------------");
                    System.out.println(" Input salah. silahkan ulangi");
                    break;
            }
            break;
            case 4:
                break out;        
            default:
                System.out.println(" ---------------------------------");
                System.out.println(" Input salah. silahkan ulangi");
                break;
            }
        }
    }
}