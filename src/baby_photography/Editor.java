package baby_photography;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Objects;

public class Editor extends Karyawan {
    public int ID_KH;
    public String Jenis, Keterangan;
    
    //ATRIBUT KEAHLIAN EDITOR
    public int ID_ED;
    String Tugas;
    
    //MEMBUAT ARRAY KEAHLIAN 
    ArrayList<Editor> AHLI = new ArrayList<>();
    
    //MEMBUAT ARRAY KEAHLIAN 
    ArrayList<Editor> PilihAhli = new ArrayList<>();
    
    //MEMBUAT ARRAY EDITOR
    ArrayList<Editor> DataEditor = new ArrayList<>();
    
    //MEMBUAT SCANNER INPUT
    Scanner input = new Scanner(System.in);

    //SETTER KEAHLIAN 
    public void setID_KH(int ID_KH) {
        this.ID_KH = ID_KH;
    }
    public void setJenis(String Jenis) {
        this.Jenis = Jenis;
    }
    public void setKeterangan(String Keterangan) {
        this.Keterangan = Keterangan;
    }
    
    //GETTER KEAHLIAN
    public int getID_KH() {
        return ID_KH;
    }
    public String getJenis() {
        return Jenis;
    }
    public String getKeterangan() {
        return Keterangan;
    } 
    
    //GETTER EDITOR
    public int getID_ED() {
        return ID_ED;
    }
    public String getTugas() {
        return Tugas;
    }
    
    //SETTER EDITOR
    public void setID_ED(int ID_ED) {
        this.ID_ED = ID_ED;
    }
    public void setTugas(String Tugas) {
        this.Tugas = Tugas;
    }
    
    public void InsertEdit(){
        System.out.println(" ---------------------------------");
        System.out.print(" Input data sebanyak : ");
        int jumlah = input.nextInt();
        for (int a=0; a<jumlah; a++){
            //ADD ADALAH FUNGSI UNTUK MENAMBAH DATA KE DALAM ARRAY LIST
            DataEditor.add(0+a, new Editor());
            
            //FFLUSH INPUT
            input.nextLine();
                
            System.out.println(" Data Editor");
            System.out.println(" -----------");
            System.out.print(" ID      : ");
            DataEditor.get(a).setID_ED(input.nextInt());
            //FFLUSH INPUT
            input.nextLine();
            System.out.print(" Nama       : ");
            DataEditor.get(a).setNama(input.nextLine());
            System.out.print(" Tugas      : ");
            DataEditor.get(a).setTugas(input.nextLine());
            System.out.print(" Agama      : ");
            DataEditor.get(a).setAgama(input.nextLine());
            System.out.print(" Alamat     : ");
            DataEditor.get(a).setAlamat(input.nextLine());
            System.out.print(" Telepon    : ");
            DataEditor.get(a).setTelp(input.nextLine());
        }
    }
    public void ReadEdit(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (DataEditor.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih kosong..........");
        }
        else {
            //SIZE ADALAH FUNGSI UNTUK MENGUKUR UKURAN DARI ARRAY LIST
            for (int b=0; b<DataEditor.size(); b++){
                System.out.println(" -----------");
                System.out.println(" Data Editor ke " + (1+b));
                System.out.println(" -----------");
                System.out.println(" ID      : " + DataEditor.get(b).getID_ED());
                System.out.println(" Nama    : " + DataEditor.get(b).getNama());
                System.out.println(" Tugas   : " + DataEditor.get(b).getTugas());
                System.out.println(" Agama   : " + DataEditor.get(b).getAgama());
                System.out.println(" Alamat  : " + DataEditor.get(b).getAlamat());
                System.out.println(" Telepon : " + DataEditor.get(b).getTelp());
                System.out.println(" Keahlian: " + DataEditor.get(b).getID_KH());
            }
        }
    }
    public void updateEdit(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (DataEditor.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Indeks Data yang ingin dirubah: ");
            int indeks = input.nextInt();
            System.out.println(" ---------------------------------");
            System.out.print(" ID      : ");
            DataEditor.get(indeks).setID_ED(input.nextInt());
            //FFLUSH INPUT
            input.nextLine();
            System.out.print(" Nama    : ");
            DataEditor.get(indeks).setNama(input.nextLine());
            System.out.print(" Tugas   : ");
            DataEditor.get(indeks).setTugas(input.nextLine());
            System.out.print(" Agama   : ");
            DataEditor.get(indeks).setAgama(input.nextLine());
            System.out.print(" Alamat  : ");
            DataEditor.get(indeks).setAlamat(input.nextLine());
            System.out.print(" Telepon : ");
            DataEditor.get(indeks).setTelp(input.nextLine());
        }
    }
    public void SearchEdit(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (DataEditor.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih Kosong..........");
        }
       
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Search Nama : ");
            String key = input.nextLine();
            System.out.println(" ---------------------------------");
            for (int i=0; i<DataEditor.size();i++){
                //OBJECTS.EQUALS ADALAH FUNGSI UNTUK MEMBANDINGKAN STRING
                if (Objects.equals(key, DataEditor.get(i).getNama())){
                    System.out.println(" ID      : " + DataEditor.get(i).getID_ED());
                    System.out.println(" Nama    : " + DataEditor.get(i).getNama());
                    System.out.println(" Tugas   : " + DataEditor.get(i).getTugas());
                    System.out.println(" Agama   : " + DataEditor.get(i).getAgama());
                    System.out.println(" Alamat  : " + DataEditor.get(i).getAlamat());
                    System.out.println(" Telepon : " + DataEditor.get(i).getTelp());
                }     
            }
        }
    }
    public void SearchIDEdit(){
        //OBJECTS.EQUALS ADALAH FUNGSI UNTUK MEMBANDINGKAN STRING
        if (DataEditor.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih Kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Search ID : ");
            int key = input.nextInt();
            System.out.println(" ---------------------------------");
            //PROSES MENCARI DATA
            for (int i=0; i<DataEditor.size();i++){
                if (key == DataEditor.get(i).getID_ED()){
                    System.out.println(" ID      : " + DataEditor.get(i).getID_ED());
                    System.out.println(" Nama    : " + DataEditor.get(i).getNama());
                    System.out.println(" Agama   : " + DataEditor.get(i).getAgama());
                    System.out.println(" Alamat  : " + DataEditor.get(i).getAlamat());
                    System.out.println(" Telepon : " + DataEditor.get(i).getTelp());
                }
            }
        }  
    }
    public void DelEdit(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (DataEditor.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih Kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Indeks yang ingin di hapus : ");
            int indeks = input.nextInt();
            System.out.println(" ---------------------------------");
            //REMOVE ADALAH CONSTRUCTOR UNTUK MENGHAPUS
            DataEditor.remove(indeks);
        }
    }
    
    public void InsertKeahlian(){
        System.out.println(" ---------------------------------");
        System.out.print(" Jumlah Insert : ");
        int jumlah = input.nextInt();
        System.out.println(" ---------------------------------");
        
        for (int a=0; a<jumlah; a++){
            //ADD ADALAH FUNGSI UNTUK MENAMBAH DATA KE DALAM ARRAY LIST
            AHLI.add(a, new Editor());
            
            //FFLUSH INPUT
            input.nextLine();
            System.out.println(" Data Keahlian Editor");
            System.out.println(" -----------");
            System.out.print(" ID         : ");
            AHLI.get(a).setID_KH(input.nextInt());
            //FFLUSH INPUT
            input.nextLine();
            System.out.print(" Jenis      : ");
            AHLI.get(a).setJenis(input.nextLine());
            System.out.print(" Keterangan : ");
            AHLI.get(a).setKeterangan(input.nextLine());
        }
    }
    public void ReadKeahlian(){
        for (int b=0; b<AHLI.size(); b++){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Keahlian Editor ke " + (1+b));
            System.out.println(" --------------------");
            System.out.println(" ID         : " + AHLI.get(b).getID_KH());
            System.out.println(" Jenis      : " + AHLI.get(b).getJenis());
            System.out.println(" Keterangan : " + AHLI.get(b).getKeterangan());
        }
    }
    public void listKeahlian(){
        for (int b=0; b<AHLI.size(); b++){
            System.out.println(" ---------------------------------");
            System.out.println(" Keahlian " + (1+b));
            System.out.println(" --------------------");
            System.out.println(" ID         : " + AHLI.get(b).getID_KH());
            System.out.println(" Jenis      : " + AHLI.get(b).getJenis());
            System.out.println(" Keterangan : " + AHLI.get(b).getKeterangan());
        }
    }
    public void updateDataKeahlian(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (AHLI.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Indeks Data yang ingin dirubah: ");
            int indeks = input.nextInt();
            System.out.println(" ---------------------------------");
            System.out.print(" ID         : ");
            AHLI.get(indeks).setID_KH(input.nextInt());
            //FFLUSH INPUT
            input.nextLine();
            System.out.print(" Jenis      : ");
            AHLI.get(indeks).setJenis(input.nextLine());
            System.out.print(" Keterangan : ");
            AHLI.get(indeks).setKeterangan(input.nextLine());
        }
    }
    public void SearchKeahlian(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (AHLI.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih Kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Search Jenis : ");
            String key = input.nextLine();
            System.out.println(" ---------------------------------");
            for (int i=0; i<AHLI.size();i++){
                //OBJECTS.EQUALS ADALAH FUNGSI UNTUK MEMBANDINGKAN STRING
                if (Objects.equals(key, AHLI.get(i).getJenis())){
                    System.out.println(" ID         : " + AHLI.get(i).getID_KH());
                    System.out.println(" Jenis      : " + AHLI.get(i).getJenis());
                    System.out.println(" Keterangan : " + AHLI.get(i).getKeterangan());
                } 
            }
        }
    }
    public void SearchIDKeahlian(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (AHLI.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih Kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Search ID : ");
            int key = input.nextInt();
            System.out.println(" ---------------------------------");
            for (int i=0; i<AHLI.size();i++){
                //PROSES MENCARI DATA
                if (key == AHLI.get(i).getID_KH()){
                    System.out.println(" ID         : " + AHLI.get(i).getID_KH());
                    System.out.println(" Jenis      : " + AHLI.get(i).getJenis());
                    System.out.println(" Keterangan : " + AHLI.get(i).getKeterangan());
                }   
            }
        }
    }
    public void DelKeahlian(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (AHLI.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih Kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Indeks yang ingin di hapus : ");
            int indeks = input.nextInt();
            System.out.println(" ---------------------------------");
            //REMOVE ADALAH CONSTRUCTOR UNTUK MENGHAPUS
            AHLI.remove(indeks);
        }
    }
}