package baby_photography;
public class Karyawan { 
    protected String Nama, Alamat, Agama, Telp;
    //SETTER
    public String getNama() {
        return Nama;
    }
    public void setNama(String Nama) {
        this.Nama = Nama;
    }
    public String getAlamat() {
        return Alamat;
    }
    public void setAlamat(String Alamat) {
        this.Alamat = Alamat;
    }
    public String getAgama() {
        return Agama;
    }
    public void setAgama(String Agama) {
        this.Agama = Agama;
    }
    //GETTER
    public String getTelp() {
        return Telp;
    }
    public void setTelp(String Telp) {
        this.Telp = Telp;
    }
}