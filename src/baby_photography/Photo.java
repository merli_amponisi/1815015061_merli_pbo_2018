package baby_photography;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Objects;

public class Photo extends Karyawan{
    public int ID_PH;
    //MEMBUAT SCANNER INPUT
    Scanner input = new Scanner(System.in);
    //MEMBUAT ARRAY
    ArrayList<Photo> DataKaryawan = new ArrayList<>();
    //GETTER
    public int getID_PH() {
        return ID_PH;
    }
    //SETTER
    public void setID_PH(int ID_PH) {
        this.ID_PH = ID_PH;
    }

    public void Insert(){
        System.out.println(" ---------------------------------");
        System.out.print(" Input data sebanyak : ");
        int jumlah = input.nextInt();
        System.out.println(" ---------------------------------");
        for (int a=0; a<jumlah; a++){
            //ADD ADALAH FUNGSI UNTUK MENAMBAH DATA KE DALAM ARRAY LIST
            DataKaryawan.add(0+a, new Photo());
            //FFLUSH INPUT
            input.nextLine();
            System.out.println(" Data Photographer");
            System.out.println(" -----------");
            System.out.print(" ID      : ");
            DataKaryawan.get(a).setID_PH(input.nextInt());
            //FFLUSH INPUT
            input.nextLine();
            System.out.print(" Nama    : ");
            DataKaryawan.get(a).setNama(input.nextLine());
            System.out.print(" Agama   : ");
            DataKaryawan.get(a).setAgama(input.nextLine());
            System.out.print(" Alamat  : ");
            DataKaryawan.get(a).setAlamat(input.nextLine());
            System.out.print(" Telepon : ");
            DataKaryawan.get(a).setTelp(input.nextLine());
        }
    }
    
    public void Read(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (DataKaryawan.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih kosong..........");
        }
        else {
            //SIZE ADALAH FUNGSI UNTUK MENGUKUR UKURAN DARI ARRAY LIST
            for (int b=0; b<DataKaryawan.size(); b++){
                System.out.println(" -----------");
                System.out.println(" Data Photographer ke " + (1+b));
                System.out.println(" -----------");
                System.out.println(" ID      : " + DataKaryawan.get(b).getID_PH());
                System.out.println(" Nama    : " + DataKaryawan.get(b).getNama());
                System.out.println(" Agama   : " + DataKaryawan.get(b).getAgama());
                System.out.println(" Alamat  : " + DataKaryawan.get(b).getAlamat());
                System.out.println(" Telepon : " + DataKaryawan.get(b).getTelp());
            }
        }
    }
    
    public void update(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (DataKaryawan.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Indeks Data yang ingin dirubah: ");
            int indeks = input.nextInt();
            System.out.println(" ---------------------------------");
            System.out.print(" ID      : ");
            DataKaryawan.get(indeks).setID_PH(input.nextInt());
            //FFLUSH INPUT
            input.nextLine();
            System.out.print(" Nama    : ");
            DataKaryawan.get(indeks).setNama(input.nextLine());
            System.out.print(" Agama   : ");
            DataKaryawan.get(indeks).setAgama(input.nextLine());
            System.out.print(" Alamat  : ");
            DataKaryawan.get(indeks).setAlamat(input.nextLine());
            System.out.print(" Telepon : ");
            DataKaryawan.get(indeks).setTelp(input.nextLine());
        }
    }
    
    public void Search(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (DataKaryawan.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih Kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Search Nama : ");
            String key = input.nextLine();
            System.out.println(" ---------------------------------");
            for (int i=0; i<DataKaryawan.size();i++){
                //OBJECTS.EQUALS ADALAH FUNGSI UNTUK MEMBANDINGKAN STRING
                if (Objects.equals(key, DataKaryawan.get(i).getNama())){
                    System.out.println(" ID      : " + DataKaryawan.get(i).getID_PH());
                    System.out.println(" Nama    : " + DataKaryawan.get(i).getNama());
                    System.out.println(" Agama   : " + DataKaryawan.get(i).getAgama());
                    System.out.println(" Alamat  : " + DataKaryawan.get(i).getAlamat());
                    System.out.println(" Telepon : " + DataKaryawan.get(i).getTelp());
                } 
            }
        }
    }
    
    public void SearchID(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (DataKaryawan.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih Kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Search ID : ");
            int key = input.nextInt();
            System.out.println(" ---------------------------------");
            
            //PROSES MENCARI DATA
            for (int i=0; i<DataKaryawan.size();i++){
                if (key == DataKaryawan.get(i).getID_PH()){
                    System.out.println(" ID      : " + DataKaryawan.get(i).getID_PH());
                    System.out.println(" Nama    : " + DataKaryawan.get(i).getNama());
                    System.out.println(" Agama   : " + DataKaryawan.get(i).getAgama());
                    System.out.println(" Alamat  : " + DataKaryawan.get(i).getAlamat());
                    System.out.println(" Telepon : " + DataKaryawan.get(i).getTelp());
                }
            }
        }   
    }
    
    public void Del(){
        //IS EMPTY ADALAH FUNGSI UNTUK MENGECEK APAKAH ARRAY LIST KOSONG ATAU TIDAK
        if (DataKaryawan.isEmpty()){
            System.out.println(" ---------------------------------");
            System.out.println(" Data Masih Kosong..........");
        }
        else{
            System.out.println(" ---------------------------------");
            System.out.print(" Indeks yang ingin di hapus : ");
            int indeks = input.nextInt();
            System.out.println(" ---------------------------------");
            //REMOVE ADALAH CONSTRUCTOR UNTUK MENGHAPUS
            DataKaryawan.remove(indeks);
        }
    }
}